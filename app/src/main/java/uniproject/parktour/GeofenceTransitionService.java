package uniproject.parktour;



import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import uniproject.parktour.utils.Constants;

public class GeofenceTransitionService extends IntentService {

    private static final String TAG = "GeofenceTransitions";

    public GeofenceTransitionService() {
        super("GeofenceTransitionsIntentService");
    }

    /**
     * When a geofence event is triggered, extract the POI id from the geofence triggered and
     * send a broadcast message (ENTER_POI) with that id, so the broadcast receiver can handle it
     * @param intent
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        Log.e("Geofence Handle", "Handled");

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            Log.e(TAG, "Goefencing Error " + geofencingEvent.getErrorCode());
            Toast.makeText(getApplicationContext(), String.valueOf(geofencingEvent.getErrorCode()), Toast.LENGTH_SHORT).show();
            return;
        }

        Log.e("Geofence", String.valueOf(geofencingEvent.getGeofenceTransition()));
        for (Geofence event :
                geofencingEvent.getTriggeringGeofences()) {
            Log.i("Geofence Name", event.getRequestId());
        }

        int geofenceTransition = geofencingEvent.getGeofenceTransition();
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            String id = null;
            for (Geofence geofence :
                    geofencingEvent.getTriggeringGeofences()) {
                if (geofence.getRequestId().startsWith("ppp"))
                    id = geofence.getRequestId();
            }

            Intent i = new Intent(Constants.ENTER_POI);
            i.putExtra(Constants.POI_ID, id);

            getApplicationContext().sendBroadcast(i);

        } else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            Log.i("Geofence", "Exit");
        } else
            Log.i("Geofence", "Unknown");

    }


}