package uniproject.parktour.data;

import android.app.Activity;

import uniproject.parktour.data.model.POI;

/**
 * This class is necessary for maintain the independence between the logical part and the database.
 * In this case it was used firebase witch is an online database, but it's easy to change and use an
 * offline database using this implemented class.
 *
 */

public class Repository {

    public Repository() {
    }

    public void getParks(final ParkCallback callback) {
        FirebaseDatabaseHandler.listParks(callback);
    }

    public void getPois(final POICallback callback) {
        FirebaseDatabaseHandler.listPOI(callback);
    }

    public void getPoiFromId(String id, final POICallback callback) {
        FirebaseDatabaseHandler.getPoiFromID(id, callback);
    }

    public void getParkFromId(String id, final ParkCallback callback) {
        FirebaseDatabaseHandler.loadParkFromId(id, callback);
    }

    public void getPoiFromPark(String idPark, POICallback callback) {
        FirebaseDatabaseHandler.listParkPoi(idPark, callback);
    }

    public void close() {
        FirebaseDatabaseHandler.removeListeners();
    }

    public void login(Activity activity, String user, String psw, GeneralCallback generalCallback) {
        FirebaseDatabaseHandler.attemptLogin(activity, user, psw, generalCallback);
    }

    public void createPoi(POI poi, GeneralCallback generalCallback) {
        FirebaseDatabaseHandler.createPoi(poi, generalCallback);
    }

    public void deletePoi(POI poi, GeneralCallback generalCallback) {
        FirebaseDatabaseHandler.deletePoi(poi, generalCallback);

    }
}
