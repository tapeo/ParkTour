package uniproject.parktour.data;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.Map;

import uniproject.parktour.data.model.POI;
import uniproject.parktour.data.model.Park;
import uniproject.parktour.utils.Constants;


public class FirebaseDatabaseHandler {

    private static ChildEventListener eventListener;
    private static Query query;
    private static long size = 0;

    /**
     * query from database the list of parks, and convert them in objects
     * @param callback
     */
    public static void listParks(final ParkCallback callback) {

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        query = reference.child(Constants.FIREBASE_PARKS);

        query.addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                size = dataSnapshot.getChildrenCount();
                query.addChildEventListener(eventListener);
            }

            public void onCancelled(DatabaseError databaseError) {
                callback.finishLoad();
            }
        });

        eventListener = new ChildEventListener() {
            int count = 0;

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                try {
                    callback.callback(dataSnapshot.getValue(Park.class));
                    count++;

                    if (count >= size) {
                        callback.finishLoad();
                        size = 0;
                    }

                } catch (Exception e) {
                    callback.error(e.getMessage());
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                callback.finishLoad();

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                callback.finishLoad();

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                callback.finishLoad();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.error(databaseError.getMessage());
            }
        };

    }

    /**
     * query from database the poi list, and convert them in objects
     * @param callback
     */
    public static void listPOI(final POICallback callback) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        query = reference.child(Constants.FIREBASE_POIS);

        query.addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                size = dataSnapshot.getChildrenCount();
                query.addChildEventListener(eventListener);
            }

            public void onCancelled(DatabaseError databaseError) {
                callback.finishLoad();
            }
        });

        eventListener = new ChildEventListener() {

            private long count = 0;


            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                try {
                    callback.callback(dataSnapshot.getValue(POI.class));
                    count++;

                    if (count >= size) {
                        callback.finishLoad();
                        size = 0;
                    }

                } catch (Exception e) {
                    callback.error(e.getMessage());
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                callback.finishLoad();
                Log.e("Firebase", "onChildChanged");
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                callback.finishLoad();
                Log.e("Firebase", "onChildRemoved");

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                callback.finishLoad();
                Log.e("Firebase", "onChildMoved");

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("Firebase", "onCancelled");

                callback.error(databaseError.getMessage());
            }
        };
    }

    /**
     * query from database one poi specified from ID , and convert it in object
     * @param id
     * @param callback
     */
    public static void getPoiFromID(String id, final POICallback callback) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        query = reference.child(Constants.FIREBASE_POIS).child(id);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {

                    POI poi = dataSnapshot.getValue(POI.class);

                    if (poi != null)
                        callback.callback(poi);
                    else
                        callback.error("no_poi_found");

                } catch (Exception e) {
                    callback.error(e.getMessage());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.error(databaseError.getMessage());
            }
        });
    }

    /**
     * query from database the poi list of one park,specified by idPark, and convert it in object
     * @param idPark
     * @param callback
     */
    public static void listParkPoi(String idPark, final POICallback callback) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        query = reference.child(Constants.FIREBASE_PARK_POI).child(idPark);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    callback.finishLoad();
                } else {
                    size = dataSnapshot.getChildrenCount();
                    query.addChildEventListener(eventListener);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.error(databaseError.getMessage());
            }
        });

        eventListener = new ChildEventListener() {

            long count = 0;

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                Query query = reference.child(Constants.FIREBASE_POIS).child(dataSnapshot.getKey());
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try {
                            callback.callback(dataSnapshot.getValue(POI.class));

                            count++;

                            if (count >= size) {
                                callback.finishLoad();
                                size = 0;
                            }

                        } catch (Exception e) {
                            callback.error(e.getMessage());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        callback.error(databaseError.getMessage());
                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                callback.finishLoad();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                callback.finishLoad();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                callback.finishLoad();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.finishLoad();
            }
        };
    }

    public static void loadParkFromId(String id, final ParkCallback callback) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        query = reference.child(Constants.FIREBASE_PARKS).child(id);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {

                    Park park = dataSnapshot.getValue(Park.class);

                    if (park != null)
                        callback.callback(park);
                    else
                        callback.error("no_poi_found");

                } catch (Exception e) {
                    callback.error(e.getMessage());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.error(databaseError.getMessage());
            }
        });
    }

    public static void attemptLogin(Activity activity, String user, String psw, final GeneralCallback generalCallback) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithEmailAndPassword(user, psw)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("attemptLogin", "signInWithEmail:onComplete:" + task.isSuccessful());

                        if (!task.isSuccessful()) {
                            Log.w("attemptLogin", "signInWithEmail:failed", task.getException());
                            generalCallback.error(task.getException().getMessage());
                        } else {
                            generalCallback.success("ok");
                        }

                    }
                });
    }

    /**
     * save a created poi on the database
     * @param poi
     * @param generalCallback
     */
    public static void createPoi(final POI poi, final GeneralCallback generalCallback) {

        String name = String.valueOf(poi.getLat()) + "_" + String.valueOf(poi.getLng());

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl(Constants.FIREBASE_STORAGE_URL);
        StorageReference imagesRef = storageRef.child(name);

        imagesRef.putFile(Uri.parse(poi.getImage_url()))
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                        poi.setImage_url(downloadUrl.toString());

                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference myRef = database.getReference();

                        Map<String, Object> taskMap = new HashMap<String, Object>();
                        String id = "ppp" + System.currentTimeMillis();
                        poi.setId(id);

                        taskMap.put("pois/" + id, poi);
                        taskMap.put("parkPoi/" + poi.getPark_id() + "/" + id, true);
                        myRef.updateChildren(taskMap);

                        generalCallback.success("ok");

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        generalCallback.error(exception.getMessage());
                    }
                });
    }

    public static void deletePoi(POI poi, GeneralCallback generalCallback) {
        try {

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference();
            myRef.child(Constants.FIREBASE_POIS).child(poi.getId()).removeValue();
            myRef.child(Constants.FIREBASE_PARK_POI).child(poi.getPark_id()).child(poi.getId()).removeValue();
            generalCallback.success("ok");

        } catch (Exception e) {
            generalCallback.error(e.getMessage());
        }
    }

    public static void removeListeners() {
        if (query != null)
            query.removeEventListener(eventListener);
    }
}