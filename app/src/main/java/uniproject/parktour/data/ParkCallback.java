package uniproject.parktour.data;

import uniproject.parktour.data.model.Park;



public interface ParkCallback {
    void callback(Park park);

    void error(String message);

    void finishLoad();
}