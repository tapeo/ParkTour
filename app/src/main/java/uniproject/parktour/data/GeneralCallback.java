package uniproject.parktour.data;



public interface GeneralCallback {
    void success(String message);

    void error(String message);
}