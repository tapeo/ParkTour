package uniproject.parktour.data;

import uniproject.parktour.data.model.POI;



public interface POICallback {
    void callback(POI poi);

    void error(String message);

    void finishLoad();
}