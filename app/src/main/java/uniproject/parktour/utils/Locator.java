package uniproject.parktour.utils;

/**
 * Created by teo on 06/06/17.
 */


import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class Locator implements LocationListener, com.google.android.gms.location.LocationListener {

    static private final String LOG_TAG = "locator";

    static private final int TIME_INTERVAL = 500; // minimum time between updates in milliseconds
    static private final int DISTANCE_INTERVAL = 1; // minimum distance between updates in meters

    private Context context;
    private LocationManager locationManager;
    private int method;
    private Locator.Listener callback;
    private GoogleApiClient apiClient;

    public Locator(Context context, GoogleApiClient client) {
        super();
        this.context = context;
        this.apiClient = client;
        this.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

    }

    public boolean isLocationEnabled(){
        boolean gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (gps_enabled || network_enabled) return true;
        else return false;
    }

    public void getLocation(int method, Locator.Listener callback) {
        this.method = method;
        this.callback = callback;
        switch (this.method) {
            case 0:
                this.requestUpdates("FUSED");
                break;
            case 1:
                Log.d(LOG_TAG, "Request updates from gps provider.");
                this.requestUpdates(LocationManager.GPS_PROVIDER);
                break;
            case 2:
                Log.d(LOG_TAG, "Request updates from gps provider.");
                this.requestUpdates(LocationManager.NETWORK_PROVIDER);
                break;

        }
    }

    private void requestUpdates(String provider) {
        if (this.locationManager.isProviderEnabled(provider) || provider.equals("FUSED")) {
            if (provider.equals("FUSED")){
                LocationRequest mLocationRequest = LocationRequest.create()
                        .setSmallestDisplacement(DISTANCE_INTERVAL)
                        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                        .setInterval(TIME_INTERVAL);

                //restart location updates with the new interval
                LocationServices.FusedLocationApi.requestLocationUpdates(apiClient, mLocationRequest, this);

            } else if (provider.contentEquals(LocationManager.NETWORK_PROVIDER)) {
                Log.d(LOG_TAG, "Network connected, start listening : " + provider);
                this.locationManager.requestLocationUpdates(provider, TIME_INTERVAL, DISTANCE_INTERVAL, this);
            } else if (provider.contentEquals(LocationManager.GPS_PROVIDER)) {
                Log.d(LOG_TAG, "Mobile network connected, start listening : " + provider);
                this.locationManager.requestLocationUpdates(provider, TIME_INTERVAL, DISTANCE_INTERVAL, this);
            } else {
                Log.d(LOG_TAG, "Proper network not connected for provider : " + provider);
                this.onProviderDisabled(provider);
            }
        } else {
            this.onProviderDisabled(provider);
        }
    }

    public void cancel() {
        Log.d(LOG_TAG, "Locating canceled.");
        this.locationManager.removeUpdates(this);
        LocationServices.FusedLocationApi.removeLocationUpdates(apiClient, this);

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(LOG_TAG, "Location found : " + location.getLatitude() + ", " + location.getLongitude() + (location.hasAccuracy() ? " : +- " + location.getAccuracy() + " meters" : ""));
        this.callback.onLocationFound(location);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d(LOG_TAG, "Provider disabled : " + provider);
        if (this.method == 1
                && provider.contentEquals(LocationManager.NETWORK_PROVIDER)) {
            // Network provider disabled, try GPS
            Log.d(LOG_TAG, "Requesst updates from GPS provider, network provider disabled.");
            this.requestUpdates(LocationManager.GPS_PROVIDER);
        } else {
            this.locationManager.removeUpdates(this);
            this.callback.onLocationNotFound();
        }
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d(LOG_TAG, "Provider enabled : " + provider);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d(LOG_TAG, "Provided status changed : " + provider + " : status : " + status);
    }

    public interface Listener {
        void onLocationFound(Location location);

        void onLocationNotFound();
    }

}
