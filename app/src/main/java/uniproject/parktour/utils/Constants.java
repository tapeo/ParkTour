package uniproject.parktour.utils;

/**
 * Here define the values constant with static field accessible all over the project
 */
public class Constants {

    public static final String ENTER_POI = "enter_poi";
    public static final String EXIT_POI = "exit_poi";
    public static final String POI_ID = "poi_id";

    public static final String FIREBASE_STORAGE_URL = "gs://parktour-4dd55.appspot.com/";
    public static final String FIREBASE_PARKS = "parks";
    public static final String FIREBASE_POIS = "pois";
    public static final String FIREBASE_PARK_POI = "parkPoi";

    public static final String ADMIN = "admin";
}
