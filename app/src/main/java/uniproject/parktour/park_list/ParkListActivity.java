package uniproject.parktour.park_list;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import java.util.HashMap;

import uniproject.parktour.R;
import uniproject.parktour.data.model.Park;

public class ParkListActivity extends AppCompatActivity implements ParkListContract.View {

    ProgressBar progressBar;
    RecyclerView list;
    ListAdapter adapter;
    HashMap<String, Park> parkList = new HashMap<>();
    Button all;

    ParkListContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mPresenter = new ParkListPresenter(ParkListActivity.this, this);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        list = (RecyclerView) findViewById(R.id.list);
        all = (Button) findViewById(R.id.all);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        list.setLayoutManager(mLayoutManager);
        list.setHasFixedSize(true);
        list.setItemAnimator(new DefaultItemAnimator());

        list.addItemDecoration(new DividerItemDecoration(this, uniproject.parktour.utils.DividerItemDecoration.VERTICAL_LIST));

        adapter = new ListAdapter(ParkListActivity.this, parkList);
        list.setAdapter(adapter);

        progressBar.setVisibility(View.VISIBLE);

        mPresenter.onCreate();

        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.allClicked();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    @Override
    public void fillData(Park park) {
        progressBar.setVisibility(View.GONE);
        parkList.put(park.getId(), park);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);

    }
}
