package uniproject.parktour.park_list;

import android.app.Activity;
import android.preference.PreferenceManager;

import uniproject.parktour.data.ParkCallback;
import uniproject.parktour.data.Repository;
import uniproject.parktour.data.model.Park;


public class ParkListPresenter implements ParkListContract.Presenter {

    private Activity mActivity;
    private ParkListContract.View mView;
    private Repository repository;

    public ParkListPresenter(Activity activity, ParkListContract.View view) {
        this.mView = view;
        this.mActivity = activity;
        repository = new Repository();
    }

    @Override
    public void onCreate() {

        mView.showProgress();

        repository.getParks(new ParkCallback() {
            @Override
            public void callback(Park park) {
                mView.fillData(park);
            }

            @Override
            public void error(String message) {
                mView.hideProgress();
            }

            @Override
            public void finishLoad() {
                mView.hideProgress();
            }
        });
    }

    @Override
    public void onDestroy() {
        repository.close();
    }

    @Override
    public void allClicked() {
        PreferenceManager.getDefaultSharedPreferences(mActivity).edit().putString("park_id", "null").apply();
        mActivity.finish();
    }


}
