package uniproject.parktour.park_list;

import android.app.Activity;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import uniproject.parktour.R;
import uniproject.parktour.data.model.Park;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {

    Activity activity;
    HashMap<String, Park> parkList = new HashMap<>();

    public ListAdapter(Activity activity, HashMap<String, Park> parkList) {
        this.activity = activity;
        this.parkList = parkList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);

        return new MyViewHolder(itemView);
    }

    /**
     * Bind an item
     * Save the park_id locally because when the app goes in background, when come back
     * in foreground the map activity need to know which poi must be displayed: all poi or
     * poi relative to a specific park
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        List<Park> list = new ArrayList<Park>(parkList.values());

        final Park park = list.get(position);

        holder.name.setText(park.getName());

        holder.click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceManager.getDefaultSharedPreferences(activity).edit().putString("park_id", park.getId()).apply();
                activity.setResult(0, null);
                activity.finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return parkList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public LinearLayout click;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            click = (LinearLayout) view.findViewById(R.id.click);
        }
    }
}