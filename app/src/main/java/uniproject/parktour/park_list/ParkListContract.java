package uniproject.parktour.park_list;

import uniproject.parktour.data.model.Park;


public interface ParkListContract {
    interface View {
        void fillData(Park park);

        void showProgress();

        void hideProgress();
    }

    interface Presenter {
        void onCreate();

        void onDestroy();

        void allClicked();
    }
}