package uniproject.parktour;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.animation.Animation;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import java.util.LinkedList;
import java.util.List;

import uniproject.parktour.data.POICallback;
import uniproject.parktour.data.ParkCallback;
import uniproject.parktour.data.Repository;
import uniproject.parktour.data.model.POI;
import uniproject.parktour.data.model.Park;
import uniproject.parktour.utils.Compass;
import uniproject.parktour.utils.Constants;
import uniproject.parktour.utils.Locator;


public class MapsPresenter implements MapsActivityContract.Presenter, Compass.CompassCallback {

    private MapsActivityContract.View mView;
    private Activity mActivity;
    private Repository repository;
    private LinkedList<POI> poiList = new LinkedList<>();
    private GoogleApiClient mGoogleApiClient;
    private List<Geofence> geofenceList = new LinkedList<>();
    private Locator locator;
    private PendingIntent pendingIntent;

    private Compass compass;

    /**
     * receive the broadcast message (ENTER_POI) to load the poi details
     */
    private BroadcastReceiver enterPoi = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String id = intent.getStringExtra(Constants.POI_ID);
            Log.e("ENTER", id);
            mView.showPoiDetail(id);
        }
    };

    /**
     * Unused for now
     */
    private BroadcastReceiver exitPoi = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String id = intent.getStringExtra(Constants.POI_ID);
            Log.e("EXIT", id);

        }
    };

    /**
     * 0 -> Automatic location updates
     * 1 -> use GPS
     * 2 -> use NETWORK
    */
    private int gpsMode = 0;

    public MapsPresenter(Activity activity, MapsActivityContract.View view) {
        this.mView = view;
        this.mActivity = activity;
        repository = new Repository();

        compass = new Compass(activity, this);
    }

    @Override
    public void onCreate() {
        mView.initMapFragment();

        mView.requestPermissions();

        registerBroadcast();

        initGoogleAPIClient();
    }

    @Override
    public void onResume() {
        compass.start();
    }

    @Override
    public void onPause() {
        compass.stop();
    }

    @Override
    public void onDestroy() {
        repository.close();
        removeGeofences();
        unregisterBroadcast();

        if (locator != null)
            locator.cancel();
    }

    /**
     * register a broadcastreceiver to handle the IntentFilter and transition for event "ENTER_POI"
     */
    private void registerBroadcast() {
        IntentFilter enter_filter = new IntentFilter(Constants.ENTER_POI);
        IntentFilter exit_filter = new IntentFilter(Constants.EXIT_POI);
        mActivity.registerReceiver(enterPoi, enter_filter);
        mActivity.registerReceiver(exitPoi, exit_filter);
    }

    private void unregisterBroadcast() {
        mActivity.unregisterReceiver(enterPoi);
        mActivity.unregisterReceiver(exitPoi);
    }

    private void initGoogleAPIClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        Log.e("ApiGoogle", "Connected");
                        getPOI();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        mView.showMessage(mActivity.getString(R.string.impossible_to_connect));
                    }
                })
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void fabClick() {
        getLocation();
    }

    @Override
    public void requestPermissionResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length != 0) {
            if (requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mView.requestPermissions();
            } else if (requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                mView.showDialogPermission();
            }
        }
    }

    /**
     * return the user current location and update it when changes
     */
    @Override
    public void getLocation() {
        try {
            if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                mView.requestPermissions();
                return;
            }

            locator = new Locator(mActivity, mGoogleApiClient);

            if (locator.isLocationEnabled()) {

                mView.showProgressBar();

                locator.getLocation(gpsMode, new Locator.Listener() {
                    @Override
                    public void onLocationFound(Location location) {
                        mView.hideProgressBar();
                        mView.moveHereMeMarker(location.getLatitude(), location.getLongitude());
                    }

                    @Override
                    public void onLocationNotFound() {
                        mView.hideProgressBar();
                        mView.showMessage(mActivity.getString(R.string.no_location_found));
                    }
                });

            } else
                mView.alertGPS();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 0 -> AUTO
     * 1 -> use GPS
     * 2 -> use NETWORK
     *
     * Store value locally
     *
     **/
    @Override
    public void setGPSMode(int i) {
        PreferenceManager.getDefaultSharedPreferences(mActivity).edit().putInt("gps_mode_value", i).apply();
        gpsMode = i;
    }

    @Override
    public void updatePOI() {
        getPOI();
    }

    /**
     * set the poi list
     */
    @Override
    public void getPOI() {
        if (isOnline()) {
            mView.showProgressBar();

            poiList.clear();
            geofenceList.clear();
            mView.clearMarkers();
            mView.removeAllCircles();

            String parkId = PreferenceManager.getDefaultSharedPreferences(mActivity).getString("park_id", "null");

            if (parkId.equals("null"))
                getAllPois();
            else
                getSpecificParkPois(parkId);

        } else
            mView.showMessage(mActivity.getString(R.string.no_internet));

    }

    @Override
    public void getAllPois() {
        repository.getPois(new POICallback() {
            /**
             * load one poi at time in the poiList, sent by the repository
             * @param poi
             */
            @Override
            public void callback(POI poi) {
                Log.d("POI", poi.getName());
                poiList.add(poi);
                mView.addPOI(poi);
            }

            @Override
            public void error(String message) {
                Log.d("POI", message);
                mView.showMessage(message);
                mView.hideProgressBar();

            }

            @Override
            public void finishLoad() {
                mView.hideProgressBar();
                createGeofences();
            }
        });
    }

    @Override
    public void getSpecificParkPois(String parkId) {

        getParkFromIdLocation(parkId);

        repository.getPoiFromPark(parkId, new POICallback() {
            @Override
            public void callback(POI poi) {
                Log.d("POI", poi.getName());
                poiList.add(poi);
                mView.addPOI(poi);
            }

            @Override
            public void error(String message) {
                Log.d("POI", message);
                mView.showMessage(message);
                mView.hideProgressBar();
            }

            @Override
            public void finishLoad() {
                mView.hideProgressBar();
                createGeofences();
            }
        });
    }

    /**
     * load a specific park for move the camera to that selected park
     * @param parkId
     */
    @Override
    public void getParkFromIdLocation(String parkId) {
        repository.getParkFromId(parkId, new ParkCallback() {
            @Override
            public void callback(Park park) {
                mView.moveSpecificLocation(park.getLat(), park.getLng());
            }

            @Override
            public void error(String message) {
                mView.showMessage(message);
            }

            @Override
            public void finishLoad() {

            }
        });
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) mActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    /**
     * Create geofence for every poi loaded on the map, and add an enter transition event. So when the user
     * is located in the circular region of 200 meters, the event will be handled by GeofenceTransitionService class
     */
    @Override
    public void createGeofences() {

        geofenceList.clear();
        mView.removeAllCircles();

        for (POI poi : poiList) {
            Log.d("Lat Fence", poi.getName() + " - " + String.valueOf(poi.getLat()));
            Log.d("Lng Fence", String.valueOf(poi.getLng()));

            Geofence fence = new Geofence.Builder()
                    .setRequestId(poi.getId())
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                    .setCircularRegion(poi.getLat(), poi.getLng(), 200)
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .build();

            CircleOptions circleOptions = new CircleOptions()
                    .center(new LatLng(poi.getLat(), poi.getLng()) )
                    .radius(200)
                    .fillColor(0x40087C39)
                    .strokeColor(Color.TRANSPARENT)
                    .strokeWidth(2);

            mView.drawCircle(circleOptions);

            geofenceList.add(fence);

            if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }

        if (!geofenceList.isEmpty())
            LocationServices.GeofencingApi.addGeofences(
                    mGoogleApiClient,
                    getGeofencingRequest(),
                    getGeofencePendingIntent()
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    if (status.isSuccess()) {
                        Log.e("createGeofences", "Saving Geofence");

                    } else {
                        Log.e("createGeofences", "Registering geofence failed: " + status.getStatusMessage() +
                                " : " + status.getStatusCode());
                    }
                }
            });
    }

    @Override
    public void removeGeofences() {
        if (pendingIntent != null)
            LocationServices.GeofencingApi.removeGeofences(mGoogleApiClient, pendingIntent);
    }

    /**
     * Retrieve default value of gps mode and pass to the alert dialog
     */
    @Override
    public void gpsModeClicked() {
        int defaultValue = PreferenceManager.getDefaultSharedPreferences(mActivity).getInt("gps_mode_value", 0);
        mView.showGPSMode(defaultValue);
    }

    /**
     * set the geofence list to a geofence request with a INITIAL_TRIGGER_ENTER transition
     * @return
     */
    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(geofenceList);
        return builder.build();

    }

    /**
     * create the pending intent to instantiate the class that will be triggered
     * @return
     */
    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (pendingIntent != null) {
            return pendingIntent;
        }
        Intent intent = new Intent(mActivity, GeofenceTransitionService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        pendingIntent = PendingIntent.getService(mActivity, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return pendingIntent;
    }

    @Override
    public void getRotation(Animation rotation) {
        mView.setCompassRotation(rotation);
    }
}
