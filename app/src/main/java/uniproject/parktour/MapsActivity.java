package uniproject.parktour;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.LinkedList;

import uniproject.parktour.admin.AdminActivity;
import uniproject.parktour.data.model.POI;
import uniproject.parktour.detail.PoiDetailActivity;
import uniproject.parktour.park_list.ParkListActivity;
import uniproject.parktour.utils.Constants;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, MapsActivityContract.View {

    private static final int LOAD_PARK_CODE = 201;
    private MapsPresenter mPresenter;

    private GoogleMap mMap;
    private FloatingActionButton fab;
    private ProgressBar progressBar;
    private Marker currentLocation;
    private HashMap<Marker, POI> markerList = new HashMap<Marker, POI>();
    private ImageView compass;
    private LinkedList<Circle> circles = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mPresenter = new MapsPresenter(MapsActivity.this, this);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        compass = (ImageView) findViewById(R.id.compass);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.fabClick();
            }
        });

        mPresenter.onCreate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.onPause();
    }

    /**
     * Initialize map fragment
     */
    @Override
    public void initMapFragment() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Map ready
     * Setup settings
     * Init listener for marker click
     *
     * @param googleMap
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setTiltGesturesEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setRotateGesturesEnabled(false);

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                POI poi = markerList.get(marker);
                if (poi != null)
                    showPoiDetail(poi.getId());
                return true;
            }
        });

        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(new LatLng(45.818669, 8.823825), 12);
        mMap.animateCamera(location);
    }

    /**
     * move the user marker to the current location
     * @param lat
     * @param lng
     */
    @Override
    public void moveHereMeMarker(double lat, double lng) {
        LatLng latLng = new LatLng(lat, lng);

        if (currentLocation != null)
            currentLocation.remove();

        currentLocation = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_me))
                .title(getString(R.string.me)));

        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(latLng, 17);
        mMap.animateCamera(location);
    }

    /**
     * move the camera on specific map point
     * @param lat
     * @param lng
     */
    @Override
    public void moveSpecificLocation(double lat, double lng) {
        LatLng latLng = new LatLng(lat, lng);

        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(latLng, 15);
        mMap.animateCamera(location);
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void alertGPS() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapsActivity.this);
        alertDialog.setTitle(R.string.gps_not_enabled);
        alertDialog.setMessage(R.string.want_enable_gps);
        alertDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });
        alertDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    /**
     * open the gps choose menu in the options menu and allow the user to choose
     * which location type can be used by the app
     */
    @Override
    public void showGPSMode(int defaultValue) {
        final CharSequence[] items = {getString(R.string.auto), getString(R.string.gps), getString(R.string.network)};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.gps_mode);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setSingleChoiceItems(items, defaultValue, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        mPresenter.setGPSMode(0);
                        break;
                    case 1:
                        mPresenter.setGPSMode(1);
                        break;
                    case 2:
                        mPresenter.setGPSMode(2);
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * remove all the markers from the map marker list
     */
    @Override
    public void clearMarkers() {
        for (Marker m :
                markerList.keySet()) {
            m.remove();
        }

        markerList.clear();
    }

    /**
     * add one marker in the map
     * @param poi
     */
    @Override
    public void addPOI(POI poi) {
        double latitude = poi.getLat();
        double longitude = poi.getLng();
        Marker marker = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.poi)));
        marker.setTitle(poi.getName());

        markerList.put(marker, poi);
    }

    /**
     * start the details point activity relative to the specified poi
     * @param poiID
     */
    @Override
    public void showPoiDetail(String poiID) {
        Intent intent = new Intent(this, PoiDetailActivity.class);
        intent.putExtra(Constants.POI_ID, poiID);
        startActivity(intent);
    }

    @Override
    public void setCompassRotation(Animation rotation) {
        compass.startAnimation(rotation);
    }

    @Override
    public void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showDialogPermission() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapsActivity.this);
        alertDialog.setTitle(R.string.permissions);
        alertDialog.setMessage(R.string.permission_descritpion);
        alertDialog.setPositiveButton(R.string.allow, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                requestPermissions();
            }
        });
        alertDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.show();
    }

    /**
     * draw circle for geofence perimeter
     * @param circleOptions
     */
    @Override
    public void drawCircle(CircleOptions circleOptions) {
        circles.add(mMap.addCircle(circleOptions));
    }

    @Override
    public void removeAllCircles() {
        for (Circle c :
                circles) {
            c.remove();
        }
    }

    @Override
    public void requestPermissions() {
        if (ContextCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

        if (ContextCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        if (ContextCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPresenter.requestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.gps_mode:
                mPresenter.gpsModeClicked();
                return true;
            case R.id.update:
                mPresenter.updatePOI();
                return true;
            case R.id.list:
                Intent intent = new Intent(this, ParkListActivity.class);
                startActivityForResult(intent, LOAD_PARK_CODE);
                return false;
            case R.id.admin:
                Intent intent2 = new Intent(this, AdminActivity.class);
                startActivity(intent2);
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /**
     * load markers relative to a specific park when a park from the park list is clicked
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOAD_PARK_CODE && resultCode == 0) {
            mPresenter.updatePOI();
        }
    }
}
