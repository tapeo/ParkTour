package uniproject.parktour;

import android.view.animation.Animation;

import com.google.android.gms.maps.model.CircleOptions;

import uniproject.parktour.data.model.POI;



public interface MapsActivityContract {
    interface View {
        void requestPermissions();

        void initMapFragment();

        void alertGPS();

        void moveHereMeMarker(double lat, double lng);

        void moveSpecificLocation(double lat, double lng);

        void showProgressBar();

        void hideProgressBar();

        void showGPSMode(int defaultValue);

        void clearMarkers();

        void addPOI(POI poi);

        void showPoiDetail(String poiID);

        void setCompassRotation(Animation rotation);

        void showMessage(String s);

        void showDialogPermission();

        void drawCircle(CircleOptions circleOptions);

        void removeAllCircles();
    }

    interface Presenter {
        void onCreate();

        void onResume();

        void onPause();

        void onDestroy();

        void fabClick();

        void requestPermissionResult(int requestCode, String[] permissions, int[] grantResults);

        void getLocation();

        void setGPSMode(int i);

        void updatePOI();

        void getPOI();

        void getAllPois();

        void getSpecificParkPois(String parkId);

        void getParkFromIdLocation(String parkId);

        void createGeofences();

        void removeGeofences();

        void gpsModeClicked();
    }
}