package uniproject.parktour.admin.create;

import android.net.Uri;

import com.google.android.gms.maps.model.LatLng;

import java.util.LinkedList;

/**
 * Created by teo on 06/06/17.
 */

public interface CreatePoiContract {
    interface View {
        void showMessage(String message);

        void showProgress();

        void hideProgress();

        void populateSpinner(LinkedList<String> parks);

        void showConfirm();
    }

    interface Presenter {
        void onCreate();

        void onStart();

        void onStop();

        void acceptClicked();

        void setPoiImageUri(Uri imageUri);

        void setPoiName(String txt);

        void setPoiDescription(String txt);

        void setPoiLatLng(LatLng latLng);

        void selectParkItem(int item);

        void poiConfirmed();
    }
}