package uniproject.parktour.admin.create;

import android.app.Activity;
import android.net.Uri;

import com.google.android.gms.maps.model.LatLng;

import java.util.LinkedList;

import uniproject.parktour.R;
import uniproject.parktour.data.GeneralCallback;
import uniproject.parktour.data.ParkCallback;
import uniproject.parktour.data.Repository;
import uniproject.parktour.data.model.POI;
import uniproject.parktour.data.model.Park;

/**
 * Created by teo on 06/06/17.
 */

public class CreatePoiPresenter implements CreatePoiContract.Presenter {

    private Activity mActivity;
    private CreatePoiContract.View mView;
    private Repository repository;

    private LinkedList<Park> parks = new LinkedList<>();
    private LinkedList<String> parks_string = new LinkedList<>();

    private Uri imageUri;
    private String name, description;
    private LatLng latLng;
    private String parkId;

    public CreatePoiPresenter(Activity activity, CreatePoiContract.View view) {
        this.mView = view;
        this.mActivity = activity;
        repository = new Repository();

    }

    @Override
    public void onCreate() {

        parks.clear();

        repository.getParks(new ParkCallback() {
            @Override
            public void callback(Park park) {
                parkId = park.getId();

                parks.add(park);
                parks_string.add(park.getName());

                mView.populateSpinner(parks_string);
            }

            @Override
            public void error(String message) {
                mView.showMessage(message);
            }

            @Override
            public void finishLoad() {

            }
        });
    }

    @Override
    public void onStart() {
    }

    @Override
    public void onStop() {
    }

    /**
     * user accept to add a specific poi
     */
    @Override
    public void acceptClicked() {
        if (imageUri != null && name != null && description != null && latLng != null && parkId != null) {
            mView.showConfirm();
        } else
            mView.showMessage(mActivity.getString(R.string.fields_not_valid));
    }

    @Override
    public void setPoiImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }

    @Override
    public void setPoiName(String txt) {
        name = txt;
    }

    @Override
    public void setPoiDescription(String txt) {
        description = txt;
    }

    @Override
    public void setPoiLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    @Override
    public void selectParkItem(int item) {
        parkId = parks.get(item).getId();
    }

    /**
     * create the POJO poi and upload to the database
     */
    @Override
    public void poiConfirmed() {
        POI poi = new POI();
        poi.setDescription(description);
        poi.setImage_url(imageUri.toString());
        poi.setName(name);
        poi.setLat(latLng.latitude);
        poi.setLng(latLng.longitude);
        poi.setPark_id(parkId);

        mView.showProgress();

        repository.createPoi(poi, new GeneralCallback() {
            @Override
            public void success(String message) {
                mView.hideProgress();
                mView.showMessage(mActivity.getString(R.string.poi_created));
                mActivity.finish();
            }

            @Override
            public void error(String message) {
                mView.hideProgress();
                mView.showMessage(message);
            }
        });
    }

}
