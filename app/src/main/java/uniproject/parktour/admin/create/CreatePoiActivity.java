package uniproject.parktour.admin.create;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.LinkedList;

import uniproject.parktour.R;

public class CreatePoiActivity extends AppCompatActivity implements CreatePoiContract.View, GoogleApiClient.OnConnectionFailedListener {

    private CreatePoiContract.Presenter mPresenter;
    private ProgressBar progress;
    private ImageView image;
    private Button name, description, location;
    private FloatingActionButton fab;
    private Spinner spinner_park;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poi_creation);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        progress = (ProgressBar) findViewById(R.id.progress);
        name = (Button) findViewById(R.id.name);
        description = (Button) findViewById(R.id.description);
        location = (Button) findViewById(R.id.location);
        image = (ImageView) findViewById(R.id.image);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        spinner_park = (Spinner) findViewById(R.id.parks);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.acceptClicked();
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, getString(R.string.select_image)), 101);
            }
        });

        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(CreatePoiActivity.this);
                final View edittext = inflater.inflate(R.layout.edit_text, null);

                AlertDialog.Builder alert = new AlertDialog.Builder(CreatePoiActivity.this);
                alert.setTitle(R.string.poi);
                alert.setView(edittext);
                alert.setPositiveButton(R.string.set, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        EditText et = (EditText) edittext.findViewById(R.id.input);
                        et.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

                        name.setText(et.getText().toString());
                        mPresenter.setPoiName(et.getText().toString());

                    }
                });
                alert.show();
            }
        });

        description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(CreatePoiActivity.this);
                final View edittext = inflater.inflate(R.layout.edit_text, null);

                AlertDialog.Builder alert = new AlertDialog.Builder(CreatePoiActivity.this);
                alert.setTitle(R.string.poi);
                alert.setView(edittext);
                alert.setPositiveButton(R.string.set, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        EditText et = (EditText) edittext.findViewById(R.id.input);
                        et.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

                        description.setText(et.getText().toString());
                        mPresenter.setPoiDescription(et.getText().toString());

                    }
                });
                alert.show();
            }
        });

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(CreatePoiActivity.this), 102);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        spinner_park.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mPresenter.selectParkItem(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mPresenter = new CreatePoiPresenter(CreatePoiActivity.this, this);
        mPresenter.onCreate();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);

    }

    /**
     * populate the list park in the spinner
     * @param parks
     */
    @Override
    public void populateSpinner(LinkedList<String> parks) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, parks);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_park.setAdapter(adapter);
    }

    @Override
    public void showConfirm() {
        AlertDialog.Builder alert = new AlertDialog.Builder(CreatePoiActivity.this);
        alert.setTitle(R.string.poi);
        alert.setMessage(R.string.confirm_poi_creation);
        alert.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mPresenter.poiConfirmed();
            }
        });
        alert.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            if (data != null) {
                Uri uri = data.getData();
                mPresenter.setPoiImageUri(uri);
                image.setImageURI(uri);
                image.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        } else if (requestCode == 102) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                location.setText(place.getLatLng().toString());
                mPresenter.setPoiLatLng(place.getLatLng());
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        showMessage(getString(R.string.impossible_to_connect));
    }
}
