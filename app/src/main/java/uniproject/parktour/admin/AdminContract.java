package uniproject.parktour.admin;

public interface AdminContract {
    interface View {
        void showMessage(String message);

        void showProgress();

        void hideProgress();

        void startListPois();
    }

    interface Presenter {
        void loginClicked(String user, String psw);
    }
}