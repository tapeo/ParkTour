package uniproject.parktour.admin.poi_list;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import uniproject.parktour.R;
import uniproject.parktour.data.model.POI;
import uniproject.parktour.detail.PoiDetailActivity;
import uniproject.parktour.utils.Constants;

public class PoiListAdapter extends RecyclerView.Adapter<PoiListAdapter.MyViewHolder> {

    Activity activity;
    HashMap<String, POI> poiList = new HashMap<>();

    public PoiListAdapter(Activity activity, HashMap<String, POI> poiList) {
        this.activity = activity;
        this.poiList = poiList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        List<POI> list = new ArrayList<POI>(poiList.values());

        final POI poi = list.get(position);

        holder.name.setText(poi.getName());

        holder.click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, PoiDetailActivity.class);
                intent.putExtra(Constants.POI_ID, poi.getId());
                intent.putExtra(Constants.ADMIN, true);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return poiList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public LinearLayout click;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            click = (LinearLayout) view.findViewById(R.id.click);
        }
    }
}