package uniproject.parktour.admin.poi_list;

import android.app.Activity;
import android.util.Log;

import java.util.LinkedList;

import uniproject.parktour.data.POICallback;
import uniproject.parktour.data.ParkCallback;
import uniproject.parktour.data.Repository;
import uniproject.parktour.data.model.POI;
import uniproject.parktour.data.model.Park;

/**
 * Created by teo on 06/06/17.
 */

public class PoiListPresenter implements PoiListContract.Presenter {

    private Activity mActivity;
    private PoiListContract.View mView;
    private Repository repository;
    private LinkedList<Park> parks = new LinkedList<>();
    private LinkedList<String> parks_string = new LinkedList<>();

    public PoiListPresenter(Activity activity, PoiListContract.View view) {
        this.mView = view;
        this.mActivity = activity;
        repository = new Repository();
    }

    @Override
    public void onCreate() {

        parks.clear();
        parks_string.clear();
        mView.clearSpinner();

        repository.getParks(new ParkCallback() {
            @Override
            public void callback(Park park) {
                parks.add(park);
                parks_string.add(park.getName());

                mView.populateSpinner(parks_string);
            }

            @Override
            public void error(String message) {
                mView.showMessage(message);
            }

            @Override
            public void finishLoad() {

            }
        });
    }

    @Override
    public void onDestroy() {
        repository.close();
    }

    @Override
    public void onResume() {

    }

    @Override
    public void parkSelected(int i) {

        mView.clearList();
        mView.showProgress();

        repository.getPoiFromPark(parks.get(i).getId(), new POICallback() {
            @Override
            public void callback(POI poi) {
                mView.fillData(poi);
            }

            @Override
            public void error(String message) {
                Log.d("POI", message);
                mView.showMessage(message);
                mView.hideProgress();
            }

            @Override
            public void finishLoad() {
                mView.hideProgress();
            }
        });
    }

}
