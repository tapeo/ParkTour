package uniproject.parktour.admin.poi_list;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.HashMap;
import java.util.LinkedList;

import uniproject.parktour.R;
import uniproject.parktour.admin.create.CreatePoiActivity;
import uniproject.parktour.data.model.POI;

public class PoiListActivity extends AppCompatActivity implements PoiListContract.View {

    ProgressBar progressBar;
    RecyclerView list;
    PoiListAdapter adapter;
    HashMap<String, POI> poiList = new HashMap<>();
    FloatingActionButton fab;
    SwipeRefreshLayout refresh;
    Spinner spinner;

    PoiListContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_poi);

        mPresenter = new PoiListPresenter(PoiListActivity.this, this);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        list = (RecyclerView) findViewById(R.id.list);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        refresh = (SwipeRefreshLayout) findViewById(R.id.refresh);
        spinner = (Spinner) findViewById(R.id.parks);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CreatePoiActivity.class);
                startActivity(intent);
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        list.setLayoutManager(mLayoutManager);
        list.setHasFixedSize(true);
        list.setItemAnimator(new DefaultItemAnimator());
        list.addItemDecoration(new DividerItemDecoration(this, uniproject.parktour.utils.DividerItemDecoration.VERTICAL_LIST));

        adapter = new PoiListAdapter(PoiListActivity.this, poiList);
        list.setAdapter(adapter);

        progressBar.setVisibility(View.VISIBLE);

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh.setRefreshing(false);
                poiList.clear();
                mPresenter.onCreate();
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mPresenter.parkSelected(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mPresenter.onCreate();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    @Override
    public void fillData(POI poi) {
        progressBar.setVisibility(View.GONE);
        poiList.put(poi.getId(), poi);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clearList() {
        poiList.clear();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void clearSpinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, new String[]{});
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    @Override
    public void populateSpinner(LinkedList<String> parks) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, parks);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);

    }
}
