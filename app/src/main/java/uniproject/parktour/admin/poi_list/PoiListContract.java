package uniproject.parktour.admin.poi_list;

import java.util.LinkedList;

import uniproject.parktour.data.model.POI;

/**
 * Created by teo on 06/06/17.
 */

public interface PoiListContract {
    interface View {
        void fillData(POI poi);

        void showProgress();

        void hideProgress();

        void populateSpinner(LinkedList<String> parks_string);

        void showMessage(String message);

        void clearList();

        void clearSpinner();
    }

    interface Presenter {
        void onCreate();

        void onDestroy();

        void onResume();

        void parkSelected(int i);
    }
}