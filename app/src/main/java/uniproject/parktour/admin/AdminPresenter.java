package uniproject.parktour.admin;

import android.app.Activity;

import uniproject.parktour.R;
import uniproject.parktour.data.GeneralCallback;
import uniproject.parktour.data.Repository;



public class AdminPresenter implements AdminContract.Presenter {

    private Activity mActivity;
    private AdminContract.View mView;
    private Repository repository;

    public AdminPresenter(Activity activity, AdminContract.View view) {
        this.mView = view;
        this.mActivity = activity;
        repository = new Repository();
    }

    /**
     * Try to login the user with inserted email and psw
     * @param user
     * @param psw
     */
    @Override
    public void loginClicked(String user, String psw) {
        if (user.length() > 0 && psw.length() > 0) {

            mView.showProgress();

            repository.login(mActivity, user, psw, new GeneralCallback() {
                @Override
                public void success(String message) {
                    mView.hideProgress();
                    mView.startListPois();
                }

                @Override
                public void error(String message) {
                    mView.showMessage(message);
                    mView.hideProgress();
                }
            });
        } else
            mView.showMessage(mActivity.getString(R.string.fields_not_valid));
    }


}
