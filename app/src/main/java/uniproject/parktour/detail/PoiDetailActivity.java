package uniproject.parktour.detail;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import uniproject.parktour.R;
import uniproject.parktour.utils.Constants;

public class PoiDetailActivity extends AppCompatActivity implements PoiDetailContract.View {

    private PoiDetailContract.Presenter mPresenter;
    private ImageView image;
    private ProgressBar progress;
    private TextView name, description, lat, lng;
    private boolean admin;
    private String poiId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poi_detail);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        image = (ImageView) findViewById(R.id.image);
        progress = (ProgressBar) findViewById(R.id.progress);
        name = (TextView) findViewById(R.id.name);
        description = (TextView) findViewById(R.id.description);
        lat = (TextView) findViewById(R.id.lat);
        lng = (TextView) findViewById(R.id.lng);

        mPresenter = new PoiDetailPresenter(PoiDetailActivity.this, this);

        poiId = getIntent().getStringExtra(Constants.POI_ID);
        admin = getIntent().getBooleanExtra(Constants.ADMIN, false);

        mPresenter.onCreate(poiId, admin);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (admin)
            getMenuInflater().inflate(R.menu.menu_poi, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        switch (item.getItemId()) {
            case R.id.delete:
                askDeletePOI();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showImage(String url) {
        Picasso.with(getApplicationContext()).load(url).into(image);

    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);

    }

    @Override
    public void setName(String txt) {
        name.setText(txt);
    }

    @Override
    public void setDescription(String txt) {
        description.setText(getString(R.string.description_two_points) + txt);
    }

    @Override
    public void setLat(String txt) {
        lat.setText(getString(R.string.latitude_two_points) + txt);

    }

    @Override
    public void setLng(String txt) {
        lng.setText(getString(R.string.longitude_two_points) + txt);

    }

    @Override
    public void askDeletePOI() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(PoiDetailActivity.this);
        alertDialog.setTitle(R.string.want_delete_poi);
        alertDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                mPresenter.delete(poiId);
            }
        });
        alertDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }
}
