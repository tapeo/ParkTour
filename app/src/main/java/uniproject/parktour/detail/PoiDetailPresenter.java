package uniproject.parktour.detail;

import android.app.Activity;

import uniproject.parktour.R;
import uniproject.parktour.data.GeneralCallback;
import uniproject.parktour.data.POICallback;
import uniproject.parktour.data.Repository;
import uniproject.parktour.data.model.POI;



public class PoiDetailPresenter implements PoiDetailContract.Presenter {

    private Activity mActivity;
    private PoiDetailContract.View mView;
    private Repository repository;
    private boolean admin = false;

    public PoiDetailPresenter(Activity activity, PoiDetailContract.View view) {
        this.mView = view;
        this.mActivity = activity;
        repository = new Repository();
    }

    @Override
    public void onCreate(final String poiID, boolean admin) {

        this.admin = admin;

        mView.showProgress();

        /**
         * Load the details of a specific POI
         */
        repository.getPoiFromId(poiID, new POICallback() {
            @Override
            public void callback(POI poi) {
                mView.showImage(poi.getImage_url());
                mView.setName(poi.getName());
                mView.setDescription(poi.getDescription());
                mView.setLat(String.valueOf(poi.getLat()));
                mView.setLng(String.valueOf(poi.getLng()));
                mView.hideProgress();
            }

            @Override
            public void error(String message) {
                mView.showMessage(message);
                mView.hideProgress();
            }

            @Override
            public void finishLoad() {
                mView.hideProgress();
            }
        });
    }

    /**
     * Try to delete a specific POI
     * @param poiId
     */
    @Override
    public void delete(String poiId) {

        mView.showProgress();

        repository.getPoiFromId(poiId, new POICallback() {
            @Override
            public void callback(POI poi) {
                repository.deletePoi(poi, new GeneralCallback() {
                    @Override
                    public void success(String message) {
                        mView.showMessage(mActivity.getString(R.string.poi_removed));
                        mView.hideProgress();
                        mActivity.finish();
                    }

                    @Override
                    public void error(String message) {
                        mView.showMessage(message);
                        mView.hideProgress();

                    }
                });
            }

            @Override
            public void error(String message) {
                mView.showMessage(message);
                mView.hideProgress();
            }

            @Override
            public void finishLoad() {
                mView.hideProgress();
            }
        });


    }


}
