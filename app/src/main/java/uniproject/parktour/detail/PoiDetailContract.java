package uniproject.parktour.detail;



public interface PoiDetailContract {
    interface View {
        void showImage(String url);

        void showMessage(String message);

        void showProgress();

        void hideProgress();

        void setName(String txt);

        void setDescription(String txt);

        void setLat(String lat);

        void setLng(String lng);

        void askDeletePOI();
    }

    interface Presenter {
        void onCreate(String poiId, boolean admin);

        void delete(String poiId);
    }
}